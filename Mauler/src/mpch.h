#pragma once

#include <iostream>
#include <memory>
#include <utility>
#include <algorithm>
#include <functional>

#include <string>
#include <sstream>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "Mauler\Log.h"

#ifdef MAULER_PLATFORM_WINDOWS
	#include <Windows.h>
#endif