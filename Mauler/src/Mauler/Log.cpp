#include "mpch.h"
#include "Log.h"
#include "spdlog/sinks/stdout_sinks.h"


namespace Mauler {

	std::shared_ptr<spdlog::logger> Log::s_CoreLogger;
	std::shared_ptr<spdlog::logger> Log::s_ClientLogger;

	void Log::Init() {
		spdlog::set_pattern("%^[%T] %n: %v%$"); //Timestamp, logger name and finally the message 
		s_CoreLogger = spdlog::stderr_color_mt("Mauler");
		s_CoreLogger->set_level(spdlog::level::trace);

		s_ClientLogger = spdlog::stderr_color_mt("App");
		s_ClientLogger->set_level(spdlog::level::trace);
	}

}
