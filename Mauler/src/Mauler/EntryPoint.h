#pragma once

#include "Application.h"

#ifdef MAULER_PLATFORM_WINDOWS

extern Mauler::Application* Mauler::CreateApplication();

int main(int argc, char** argv)
{
	Mauler::Log::Init();
	MAULER_CORE_WARN("Initialized log!");
	MAULER_CLIENT_INFO("Hello world!");

	auto app = Mauler::CreateApplication();
	app->Run();
	delete app;
}

#endif