#pragma once


#ifdef MAULER_PLATFORM_WINDOWS
	#ifdef MAULER_BUILD_DLL
		#define MAULER_API __declspec(dllexport)
	#else
		#define MAULER_API __declspec(dllimport)
	#endif // MAULER_BUILD_DLL
#else
	#error MAULER SUPPORTS ONLY WINDOWS YET
#endif
#ifdef MAULER_ENABLE_ASSERTS
	#define MAULER_ASSERT(x, ...) { if(!(x)) { MAULER_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak(); } }
	#define MAULER_CORE_ASSERT(x, ...) { if(!(x)) { MAULER_CORE_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak(); } }
#else
	#define MAULER_ASSERT(x, ...)
	#define MAULER_CORE_ASSERT(x, ...)
#endif
#define BIT(x) (1 << x) 