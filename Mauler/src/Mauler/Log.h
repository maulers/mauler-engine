#pragma once
#include "Core.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"


namespace Mauler {
	class MAULER_API Log
	{
	public:

		static void Init();

		inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
		inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return s_ClientLogger; }

	private:
		static std::shared_ptr<spdlog::logger> s_CoreLogger;
		static std::shared_ptr<spdlog::logger> s_ClientLogger;

	};
}

//Core log macros
#define MAULER_CORE_ERROR(...)		::Mauler::Log::GetCoreLogger()->error(__VA_ARGS__)
#define MAULER_CORE_WARN(...)		::Mauler::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define MAULER_CORE_INFO(...)		::Mauler::Log::GetCoreLogger()->info(__VA_ARGS__)
#define MAULER_CORE_TRACE(...)		::Mauler::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define MAULER_CORE_FATAL(...)		::Mauler::Log::GetCoreLogger()->fatal(__VA_ARGS__)


//Client log macros
#define MAULER_CLIENT_ERROR(...)		::Mauler::Log::GetClientLogger()->error(__VA_ARGS__)
#define MAULER_CLIENT_WARN(...)		::Mauler::Log::GetClientLogger()->warn(__VA_ARGS__)
#define MAULER_CLIENT_INFO(...)		::Mauler::Log::GetClientLogger()->info(__VA_ARGS__)
#define MAULER_CLIENT_TRACE(...)		::Mauler::Log::GetClientLogger()->trace(__VA_ARGS__)
#define MAULER_CLIENT_FATAL(...)		::Mauler::Log::GetClientLogger()->fatal(__VA_ARGS__)