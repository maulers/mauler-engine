#pragma once

#include "Mauler/Event/Event.h"

namespace Mauler {
	class MAULER_API KeyEvent : public Event
	{
		inline int GetKeyCode() { return m_KeyCode; }

		EVENT_CLASS_CATEGORY(EventCategoryInput | EventCategoryKeyboard)
	protected:
		KeyEvent(int keyCode)
			: m_KeyCode(keyCode){}

		int m_KeyCode;

	};

	class MAULER_API KeyPressedEvent : public KeyEvent {
	public:
		KeyPressedEvent(int keyCode, int repeatCount) : KeyEvent(keyCode), m_RepeatCount(repeatCount) { }

		inline int GetRepeatCount() { return m_RepeatCount; }
		
		std::string ToString() const override {
			std::stringstream ss;
			ss << "KeyPressedEvent KeyCode: " << m_KeyCode << " RepeatCount: " << m_RepeatCount;
			return ss.str();
		}

		EVENT_CLASS_TYPE(KeyPressed)
	private:
		int m_RepeatCount;
	};

	class MAULER_API KeyReleasedEvent : public KeyEvent {
	public:
		KeyReleasedEvent(int keyCode, int repeatCount) : KeyEvent(keyCode) { }

		std::string ToString() const override {
			std::stringstream ss;
			ss << "KeyPressedEvent KeyCode: " << m_KeyCode;
			return ss.str();
		}

		EVENT_CLASS_TYPE(KeyReleased)
	};
}