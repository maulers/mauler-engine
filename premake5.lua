workspace "Mauler"
	architecture "x64"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

IncludeDir = {}
IncludeDir["GLFW"] = "Mauler/vendor/GLFW/include"
include "Mauler/vendor/GLFW"

project "Mauler"
	location "Mauler"
	kind "SharedLib"
	language "C++"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	pchheader "mpch.h"
	pchsource "Mauler/src/mpch.cpp"

	files
	{
		"%{prj.name}/src/**h",
		"%{prj.name}/src/**cpp"
		
	}

	includedirs
	{
		"%{prj.name}/src",
		"%{prj.name}/vendor/spdlog/include",
		"%{IncludeDir.GLFW}"
	}
	links 
	{
		"GLFW",
		"opengl32.lib"
	}
	filter "system:windows"
		cppdialect "C++17"
		staticruntime "On"
		systemversion "latest"

		defines 
		{
			"MAULER_PLATFORM_WINDOWS",
			"MAULER_BUILD_DLL"
		}

		postbuildcommands
		{
			("{COPY} %{cfg.buildtarget.relpath} ../bin/" .. outputdir .. "/Gamebox")
		}
	filter "configurations:Debug"
		defines "MAULER_DEBUG"
		symbols "On"

	filter "configurations:Release"
		defines "MAULER_RELEASE"
		optimize "On"

	filter "configurations:Dist"
		defines "MAULER_DIST"
		optimize "On"

project "Gamebox"
	location "Gamebox"
	kind "ConsoleApp"

	language "C++"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")
	files
	{
		"%{prj.name}/src/**h",
		"%{prj.name}/src/**cpp"
	}

	includedirs
	{
		"Mauler/vendor/spdlog/include",
		"Mauler/src"
	}

	links
	{
		"Mauler"
	}
	filter "system:windows"
		cppdialect "C++17"
		staticruntime "On"
		systemversion "latest"

		defines 
		{
			"MAULER_PLATFORM_WINDOWS"
		}

	filter "configurations:Debug"
		defines "MAULER_DEBUG"
		symbols "On"

	filter "configurations:Release"
		defines "MAULER_RELEASE"
		optimize "On"

	filter "configurations:Dist"
		defines "MAULER_DIST"
		optimize "On"